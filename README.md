# likelyRecommendations
A simple project to evaluate likely.js with data from Movielens datasets.

You can find likely.js [here](https://github.com/sbyrnes/likely.js) and the datasets used can be found [here](https://grouplens.org/datasets/movielens/100k/).
